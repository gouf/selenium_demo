require 'selenium-webdriver'

driver = Selenium::WebDriver.for :firefox # ブラウザ起動

driver.get('http://localhost:3000/index.html')

p driver.find_element(xpath: '//a').click
sleep 1
driver.switch_to.window(driver.window_handles.last)
sleep 1
driver.find_element(xpath: '//a').click

str = '/Test_Path/test.txt'
driver.switch_to.window(driver.window_handles.first)
driver.find_element(xpath: '//form/input').send_keys(str)
sleep 3
driver.quit # ブラウザ終了

# 新しいウィンドウを開くボタン押下
# driver.find_element(:id, "open_new_window_button").click

# 操作対象を新しいウィンドウに切り替える
# driver.switch_to.window(@driver.window_handles.last)
